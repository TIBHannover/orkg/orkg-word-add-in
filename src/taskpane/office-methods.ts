export async function moveSelectionToEnd() {
    return new Promise(async (resolve) => {
      await Word.run(async (context) => {
  
        let originalSelection = context.document.getSelection();
        originalSelection.select('End')
      
        await context.sync();
      });
      resolve(null)
    })
  }
  
 export async function writeXML(xml) {
    return new Promise(async (resolve) => {
      await Word.run(async (context) => {
        // Create a proxy object for the document body.
        const body = context.document.body;
      
        // Queue a command to insert OOXML at the beginning of the body.
        body.insertOoxml(xml, Word.InsertLocation.end);
      
        // Synchronize the document state by executing the queued commands, and return a promise to indicate task completion.
        await context.sync();
      });
      resolve(null)
    });
  }

  export async function writeText(text : string){
    return new Promise(async (resolve) => {
      await Word.run(async (context) => {
        // Create a proxy object for the document body.
        const body = context.document.body;
      
        // Queue a command to insert text at the beginning of the current document.
        body.insertText(text, Word.InsertLocation.end);
      
        // Synchronize the document state by executing the queued commands, and return a promise to indicate task completion.
        await context.sync();
      });
      resolve(null)
    })
  }