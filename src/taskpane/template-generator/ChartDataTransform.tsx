const xlsx = require("xlsx");

export function getChartData(json: JSON): Object {
  
  // console.log(json)
  const columnData = json["columns"];
  const arrayLength = columnData.length + 1;

  //fill first row
  let row = Array(arrayLength);
  let cellIds = Array(arrayLength);
  for (let i = 0; i < arrayLength - 1; i++) {
    const columnNum = columnData[i]["number"];
    const title = columnData[i]["titles"];
    row[columnNum] = (typeof(title) === 'object' ? title[0] : title);
    cellIds[columnNum] = columnData[i]["@id"];
  }
  let ret = [row];

  //fill other rows
  const rowData = json["rows"];
  const rowCount = rowData.length;

  for (let i = 0; i < rowCount; i++) {
    ret.push(Array(arrayLength));
  }

  for (let i = 0; i < rowCount; i++) {
    const rowNum = rowData[i]["number"];
    const cellData = rowData[i]["cells"];

    ret[rowNum][0] = "Category 1";

    for (let j = 0; j < cellData.length; j++) {
      const cellId = cellData[j]["column"];
      ret[rowNum][cellIds.indexOf(cellId)] = ((cellData[j]["value"] === null) ? null : Number(cellData[j]["value"]));
    }
  }

  // [
  //     [, "Datenreihe1", "Datenreihe2", "Datenreihe3", "Datenreihe4"],
  //     ["Kategorie 1", -7, -3, -24, 23],
  //     ["Kategorie 1", -10, 1, 11, 73],
  //     ["Kategorie 1", -28, -6, 34, 100],
  //     ["Kategorie 1", 47, 10, -19, -12],
  //     ["Kategorie 1", 11, 34, 4, -50],
  //     ["Kategorie 1", -24, 128, 27, 20],
  //     ["Kategorie 1", -24, 22, 27, -10],
  //     ["Kategorie 1", 36, -12, -3, -5],
  //     ["Kategorie 1", 10, -28, 44, 1],
  //   ]

  return {
    'data': ret,
    'rowCount': rowCount,
    'colCount': cellIds.length -1,
    'description': json['label']
  };
}

//Generate the binary Excel File of the data
export function generateChartBinary(chartData : Array<Array<any>>) {
  const workbook = xlsx.utils.book_new();

  //Create the Excel File with the data
  const worksheet = xlsx.utils.aoa_to_sheet(chartData);

  xlsx.utils.book_append_sheet(workbook, worksheet, "Tabelle1");

  const arrayBuffer = xlsx.write(workbook, { bookType: "xlsx", bookSST: true, type: "array" });

  // Convert ArrayBuffer to Uint8Array
  const uint8Array = new Uint8Array(arrayBuffer);

  // Convert Uint8Array to base64
  const base64Data = uint8Array.reduce((data, byte) => data + String.fromCharCode(byte), "");
  const base64String = btoa(base64Data);

  return base64String
}
