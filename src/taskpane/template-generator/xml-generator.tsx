const boxPlotTemplate = require("./templates/boxPlot-template.hbs");
const tableTemplate = require("./templates/table-template.hbs")
import getHelpers from "./handlebarHelpers"

function generateConclusion(topics : Array<string>, pValue : number) : string{
  if (pValue === null || pValue === undefined){
    return ""
  }else{
    let topicEnum = topics[1]
    let i = 2
    while(i < topics.length -1){
      topicEnum += (", " + topics[i])
      i++
    }
    topicEnum += " and " + topics[topics.length -1] + "."

    const pValueSignificance = [(pValue < 0.01 ? " a " : " no "), (pValue < 0.01 ? " (p < 0.01) " : " (p > 0.01) ")]
    return "We found" + pValueSignificance[0] + "statistical significant difference" + pValueSignificance[1] + "between " + topicEnum
  }
}

export function generateOoXML(chartData : object, templateType : string): string {

  const valueCount = chartData['rowCount'] -1

  const helpers = getHelpers()
  //Parameter to insert into the template
  const data = {
    binaryData: chartData['binary'].slice(0),
    categoryCount: chartData['colCount'],
    columnCount: chartData['colCount'],
    rowCount: chartData['rowCount'],
    chartDescription: chartData['description'] + ". " + generateConclusion(chartData['data'][0], chartData['pValue']),
    tableName: chartData['tableName'],
    getData: () => {return chartData['data']},
    getValueCount: () => {return valueCount},

    range: helpers[0], getLetterAfterB: helpers[1], add: helpers[2], getDataName: helpers[3], 
    insertDataTemplateNTimes: helpers[4], getColumn: helpers[5], getHeader: helpers[6], 
    getItem: helpers[7], insertRow: helpers[8],
  };

  const template = getTemplate(templateType)

  //Generate the Word XML with the template
  let generatedXML = template(data);

  // console.log(generatedXML)

  return generatedXML;
}

function getTemplate(templateType : string){
  //visualisation Parameter chooses template type
  switch(templateType){
    case 'BoxPlot':
      return boxPlotTemplate
    case 'Table': 
      return tableTemplate
  }
}