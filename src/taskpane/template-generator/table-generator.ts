
function getColumnIndex(table : Array<string>, value) : number{
    for (let i = 1; i<table.length; i++){
        if (table[i][0] === value){
            return i
        }
    }
    return -1
}

export function generateTables(tablePoints : Array<object>){
    let tables = {}
    tablePoints.forEach((pointer) => {
        // const label : string = point[0] as string
        // const labelinfo = label.split('/') // Index 0: Column, 1: Table, 2: Row
        const column = pointer['Column']
        const tableName = pointer['Table']
        const row = pointer['Row']
        const value = pointer['Value']
        const point = [[column, tableName, row], value]
        const labelinfo = [column, tableName, row]
        //TODO Continue here (Renaming)

        if (tables[labelinfo[1]] === undefined){
            tables[labelinfo[1]] = (labelinfo.length > 2 ? [[null, labelinfo[0]]] : [[labelinfo[0]]])
            tables[labelinfo[1]].push((labelinfo.length > 2 ? [labelinfo[2], point[1]] : [point[1]]))
        }else{
            let categoryIndex = tables[labelinfo[1]][0].indexOf(labelinfo[0])
            if (categoryIndex == -1){
                tables[labelinfo[1]].forEach((row, index) => {
                    row.push((index == 0 ? labelinfo[0] : null))
                });
                categoryIndex = tables[labelinfo[1]][0].length -1
            }
            let colIndex = 1
            if (labelinfo.length > 2){
                colIndex = getColumnIndex(tables[labelinfo[1]], labelinfo[2])
            }
            if (colIndex == -1){
                colIndex = tables[labelinfo[1]].length
                tables[labelinfo[1]].push(new Array(tables[labelinfo[1]][0].length))
                if(labelinfo.length > 2){
                    tables[labelinfo[1]][colIndex][0] = labelinfo[2]
                }
            }
            tables[labelinfo[1]][colIndex][categoryIndex] = point[1]
        }
    })
    return tables
}