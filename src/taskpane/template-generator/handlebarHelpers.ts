import handlebars from "handlebars/runtime"

const range = (start, end) => {
    let ret = []
    for(let i = start; i<end; i++){
        ret.push(i)
    }
    return ret
}

const getLetterAfterB = (number) => {
    return String.fromCharCode('B'.charCodeAt(0) + number);
}

const add = (a, b) => {
    return a + b;
}

const getDataName = (obj, index) => {
    return obj[0][index +1]
}

const getColumn = (obj, col, rows) => {
    let ret = []
    for(let i = 1; i<=rows; i++){
        ret.push(obj[i][col])
    }
    return ret
}

const getHeader = (data) => {
    return data[0].slice(1)
}

const getItem = (arr, i) => {
    return arr[i]
}

const insertRow = (begin, row, end) => {
    let ret = ''
    let i = 1
    for(i; i < row.length -1; i++){
        ret += begin + row[i] + end + '\n'
    }
    return new handlebars.SafeString(ret + begin + row[i] + end)
}

const insertDataTemplateNTimes = (begin, middle, insert, end, n, hasData) => {
    let ret = ""
    const dataDummy = insert
    for(let i = 0; i<n; i++){
        if (hasData){
            if(!dataDummy[i]){
                continue
            }
            if(i>0){
                ret += '\n'
            }
            insert = dataDummy[i]
        }
        ret += begin + i + middle + insert + end
    }
    return new handlebars.SafeString(ret)
}

const getHelpers = () => {
    return [range, getLetterAfterB, add, getDataName, insertDataTemplateNTimes, getColumn, getHeader, getItem, insertRow]
}

export default getHelpers