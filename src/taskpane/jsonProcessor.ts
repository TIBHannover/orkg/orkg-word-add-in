import {tableHandler, handleJsonData} from "./jsonObjectHandlerFunctions"
import { writeXML, moveSelectionToEnd, writeText } from "./office-methods"
const leaderboardYAML = require('./template-generator/YAML/leaderboard.yaml')
const studentptestYAML = require('./template-generator/YAML/student_t_test.yaml')
/* global Word console */

function parameterChanger(parameters: Array<object>, index : number){

  if(index === -1){
    index = parameters.length -1
  }

  return function addParameter(param: string, value: any, role : string = "", createNewDataPoint : boolean = false){
    if(createNewDataPoint && Object.keys(parameters[parameters.length -1]['data'][parameters[parameters.length -1]['data'].length -1]).length >= 1){
      parameters[parameters.length -1]['data'].push({})
    }else{
      switch(role){
        case "data":
          const lastPos = parameters[index]['data'].length -1
          parameters[index]['data'][lastPos][param] = value
          break;
        case "renaming":
          let lastPosRename = parameters[index]['renaming'].length -1
          if(parameters[index]['renaming'][lastPosRename][param] !== undefined){
            parameters[index]['renaming'].push({})
            lastPosRename += 1
          }
          parameters[index]['renaming'][lastPosRename][param] = value
          break;
        default:
          parameters[index][param] = value
          break;
      }
    }
  }
  
}

function getSpecification(json : JSON, yamlList : Array<object>) : object{
  const templateType = (typeof(json['@type']) === 'string' ? json["@type"] : json["@type"][0])
  for(const yaml of yamlList){
    if (yaml['type'] == templateType){
      return yaml
    }
  }
  throw new Error("Missing YAML-Specification File")
}

function traverseStructure(structure : object, details : object, json : JSON, addParameter : (param : string, value: any, role? : string, createNewDataPoint? : boolean) => void , previousParameters: object = {}){
  try{
    let branchParameters = {}
    for(const [path, content] of Object.entries(structure)){
      if (content["datatype"] !== undefined && typeof(content["datatype"]) === "string"){
        const extractedData = handleJsonData(json, path, content)
        if(extractedData === null){
          for(const jsonItems of (json[path] === undefined ? json[0][path] : json[path])){
            //add empty new Parameter to avoid override of data
            addParameter(null, null, "", true)
            traverseStructure(content['data'], details, jsonItems, addParameter, previousParameters)
          }
        }else{
          //add the parameters from the current branch
          for (const [name, value] of Object.entries(extractedData)){
            if(content['insertData'] !== false){
              addParameter(name, value, content['role'])
            }
            if(content['role'] === 'data'){
              branchParameters[name] = value
            }
          }
          //add the parameters from the parent branches
          for (const [name, value] of Object.entries(previousParameters)){
            addParameter(name, value, content['role'])
          }
          previousParameters = branchParameters
        }
      }else{
        if (json[path] === undefined) {
          if(json[0][path] === undefined){
            throw new Error("Faulty JSON File")
          }else{
            traverseStructure(content, details, json[0][path], addParameter, previousParameters)
          }
        }else{
          traverseStructure(content, details, json[path], addParameter, previousParameters)
        }
      }
    }
  }catch(err){
      console.error('Error on the interpretation of a JSON File: ', err);
  }
}

async function processJSONList(jsonList : Array<JSON>, yamlList : Array<object>) {

  //need to store all needed objects and functions because the right json object order is not guarenteed
  let templateTypes: Array<string> = [];
  let parameters: Array<object> = []
  
  yamlList.push(leaderboardYAML.default)
  yamlList.push(studentptestYAML.default)

  for (const json of jsonList){

    let templateIndex = -1

    try{
      const yaml = getSpecification(json, yamlList)
      const details = yaml['details']

      //only if multiple files are required search for previous template
      if (details['multiple_files'] === true){
        templateIndex = templateTypes.indexOf(yaml['type'])
      }
      if (templateIndex === -1){
        parameters.push({
          'visualization' : details['visualization'],
          'transformToChart': details['transformToChart']
        })
        templateTypes.push(yaml['type'])
      }
      //if multile files are needed then data parameter needs to be an array of all data points
      if(templateIndex === -1){
        parameters[parameters.length -1]['data'] = [{}]
        parameters[parameters.length -1]['renaming'] = [{}]
      }else{
        parameters[parameters.length -1]['data'].push({})
        parameters[parameters.length -1]['renaming'].push({})
      }

      traverseStructure(yaml['structure'], details, json, parameterChanger(parameters, templateIndex))

    }catch(err){
      console.error('Error on the interpretation of a JSON File: ', err);
      if(templateIndex === -1){
        templateTypes.pop()
        parameters.pop()
      }
    }
  }

  // console.log(parameters)

  if (templateTypes.length === parameters.length){
    //execute all handler functions
    for(let i = 0; i < templateTypes.length; i++){
      const handler = tableHandler
      let xml = handler(parameters[i])
  
      if (typeof(xml) === 'string'){
        await writeXML(xml)
      }else{
        for(const xmlString of xml){
          await writeXML(xmlString)
        }
      }
    }
  }
}

export default processJSONList