import * as React from "react";

export interface HeaderProps {
  title: string;
  logo: string;
  message: string;
}

const Header: React.FC<HeaderProps> = (props: HeaderProps) => {
  const { title, logo, message } = props;

  const inlineStyles = {
      "display": "flex",
      "justify-content": "center",
      "align-items": "center", /* Optional: Vertikale Ausrichtung zentrieren */
      "height": "50vh", /* Optional: Vollständige Höhe des Viewports */
  }

  return (
    <div>
      <div style={inlineStyles}>
        <img width='100' src={logo} alt={title}/>
      </div>
    </div>
  );
};

export default Header;
