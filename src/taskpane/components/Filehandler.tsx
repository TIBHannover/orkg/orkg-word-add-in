import React from 'react'
import processJsonList from '../jsonProcessor';
const yaml = require('js-yaml');

function getFileExtension(filename) {
    return filename.split('.').pop().toLowerCase();
}

function parseYAML(yamlFileContent){
    try {
        // Konvertiere YAML in ein JavaScript-Objekt
        const data = yaml.load(yamlFileContent);
        // console.log(data)
      
        // Verwende die Daten wie gewünscht
        return data;
      } catch (err) {
        console.error('Fehler beim Lesen der YAML-Datei:', err);
      }
}

function readFileAsync(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        
        reader.onload = (event) => {
            resolve(event.target.result);
        };

        reader.onerror = (error) => {
            reject(error);
        };

        reader.readAsText(file);
    });
}

const Filehandler = () => {

    async function openFiles(evt) {
        var files = evt.target.files;
        let jsonList = [];
        let yamlList = [];
    
        for (var i = 0, len = files.length; i < len; i++) {
            var file = files[i];
    
            try {
                const filetext = await readFileAsync(file);
                const fileExtension = getFileExtension(file.name);

                if (fileExtension === "json"){
                    const jsonData = JSON.parse(filetext as string);
                    jsonList.push(jsonData);
                }else{
                    const yamlData = parseYAML(filetext as string)
                    yamlList.push(yamlData)
                }

            } catch (error) {
                console.error('Error reading the file:', error);
            }
        }

        processJsonList(jsonList, yamlList)
    }

    return (
        <div className='container'>
            <label htmlFor="file-upload" className="custom-file-upload">
                Upload JSON-LD Files
            </label>
            <input id="file-upload" type="file" onChange={openFiles} multiple accept=".json,.yaml"/>
        </div>
    );
};

export default Filehandler 


