import * as React from "react";
import Header from "./Header";
import Filehandler from "./Filehandler";

interface AppProps {
  title: string;
}

const App = (props: AppProps) => {

  props
  return (
    <div>
      <Header logo="assets/ORKG-Logo.png" title={props.title} message="Welcome" />
      <Filehandler />
    </div>
  );
};

export default App;
