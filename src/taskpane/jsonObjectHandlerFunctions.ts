import {generateOoXML} from "./template-generator/xml-generator";
import { generateTables} from "./template-generator/table-generator";
import { generateChartBinary} from "./template-generator/ChartDataTransform";

//Needs modification in order to work
function getChartFormat(data : Array<Array<any>>) : Array<Array<any>>{

  //replace first column
  for(let i = 0; i < data.length; i++){
    data[i][0] = "Category 1"
  }
  //convert values to Numbers
  for(let i = 1; i < data.length; i++){
    for(let j = 1; j < data[0].length; j++){
      data[i][j] = (data[i][j] === null ? null : Number(data[i][j]))
    }
  }
  
  return data
}

function tableDataRename(data : Array<Array<any>>, renameList : Array<object>) : Array<Array<any>>{

  //rename column names if id is the same as the current name 
  for(let i = 1; i < data[0].length; i++){
    for(let rename = 0; rename < renameList.length; rename++){
      if(data[0][i] === renameList[rename]['ID']){
        data[0][i] = renameList[rename]['Name']
      }
    }
  }

  return data
}

export function tableHandler(parameters : object){

  const tableDatas = generateTables(parameters['data'])

  let xmlTables : Array<string> = []

  for (const entry of Object.entries(tableDatas)){
    let tableData = entry[1] as Array<Array<any>>
    
    //rename Data with renaming information
    tableData = tableDataRename(tableData, parameters['renaming'])
    
    //for charts only
    let binary = ''
    if(parameters['transformToChart'] === true){
      tableData = getChartFormat(tableData)
      binary = generateChartBinary(tableData)
    }
    
    tableData.sort((a, b) => a[0] - b[0]);

    const xml = generateOoXML({
      data: tableData,
      binary: binary,
      rowCount: tableData.length,
      colCount: tableData[0].length -1,
      description: (parameters['Description'] !== undefined ? parameters['Description'] : ""),
      tableName: entry[0],
      pValue: (parameters['PValue'] !== undefined ? parameters['PValue'] : null)
    }, parameters['visualization'])

    xmlTables.push(xml)
  }
  return xmlTables
}

export function handleJsonData(json : JSON, path : string, specification : object){
  const datatype = specification['datatype']
  if (typeof(json[path]) === "undefined"){
    if(typeof(json[0][path]) === "undefined"){
      throw new Error('Faulty JSON File')
    }
    json = json[0]
  }
  let ret = {}
  switch(datatype){
    case 'string':
      let str
      try{
        str = (typeof(json[path]) === "object" ? json[path][0] : json[path])
      }catch(error){
        if(error instanceof TypeError){
          str = null
        }else{
          throw new Error('Something went wrong reading a cell in the JSON File')
        } 
      }
      if(specification['seperator'] !== undefined){
        const seperatedData = str.split(specification['seperator'])
        for(let i = 0; i<seperatedData.length; i++){
          ret[specification['data'][i]] = seperatedData[i]
        }
      }else{
        ret[specification['data']] = str
      }
      break;
    case 'data_collection':
      return null
    default:
      throw new Error("Unknown Datatype")
  }
  return ret
}